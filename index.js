var editorjs = new EditorJS({
    placeholder: "Enter or Type here...",

    tools: {
        header: {
          class: Header,
          inlineToolbar: ['marker', 'link'],
          config: {
            placeholder: 'Header'
          },
          shortcut: 'CTRL+SHIFT+H'
        },

        image: SimpleImage,
        
        image: {
          class: ImageTool,
          inlineToolbar: true,
        },

        list: {
          class: List,
          inlineToolbar: true,
          shortcut: 'CTRL+SHIFT+L'
        },

        checklist: {
          class: Checklist,
          inlineToolbar: true,
        },

        quote: {
          class: Quote,
          inlineToolbar: true,
          config: {
            quotePlaceholder: 'Enter a quote',
            captionPlaceholder: 'Quote\'s author',
          },
          shortcut: 'CTRL+SHIFT+O'
        },

        warning: Warning,

        marker: {
          class:  Marker,
          shortcut: 'CTRL+SHIFT+M'
        },

        code: {
          class:  CodeTool,
          shortcut: 'SHIFT+ALT+C'
        },

        delimiter: Delimiter,

        inlineCode: {
          class: InlineCode,
          shortcut: 'CTRL+SHIFT+I'
        },

        linkTool: LinkTool,

        embed: Embed,

        table: {
          class: Table,
          inlineToolbar: true,
          shortcut: 'CTRL+ALT+T'
        },

    },

    
});
function myFunction(){
    editorjs.save().then((output) => {
        console.log('Data: ', output);
    }).catch((error) => {
        console.log('Saving failed: ', error)
    });
}